<?php

function getFullAddress($country, $city, $province, $specificAddress) {
    return "$specificAddress, $city, $province, $country" ;

}

function getLetterGrade($numberGrade){
    if ( $numberGrade >= 98 && $numberGrade <= 100 ) {
        return 'A+';
    }
    else if ( $numberGrade >= 95 && $numberGrade <= 97 ) {
        return 'A';
    } else if ( $numberGrade >= 92 && $numberGrade <= 94 ) {
        return 'A-';
    } else if ( $numberGrade >= 89 && $numberGrade <= 91 ) {
        return 'B+';
    } else if ( $numberGrade >= 86 && $numberGrade <= 88 ) {
        return 'B';
    } else if ( $numberGrade >= 83 && $numberGrade <= 85 ) {
        return 'B-';
    } else if ( $numberGrade >= 80 && $numberGrade <= 82 ) {
        return 'C+';
    } else if ( $numberGrade >= 77 && $numberGrade <= 79 ) {
        return 'C';
    } else if ( $numberGrade >= 75 && $numberGrade <= 76 ) {
        return 'C-';
    } else  {
        return 'D';
    }
}