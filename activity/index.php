<?php require_once './code.php'; ?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>S1 Activity</title>
</head>
<body>
    <h1>Full Address:</h1>
    <p> <?php echo getFullAddress('United Kingdom', 'Camden', 'London', '21 Cornelia St.')?> </p>
    <p>  <?php echo getFullAddress('Philippines', 'Quezon City', 'Metro Manila', '51 Brown St.')?> </p>


    <h1>Letter-Based Grading </h1>
    <p> 100 is equivalent to: <?php echo getLetterGrade(100); ?></p>
    <p> 88 is equivalent to: <?php echo getLetterGrade(88); ?></p>
    <p> 74 is equivalent to: <?php echo getLetterGrade(75); ?></p>


</body>
</html>