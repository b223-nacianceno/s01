<!-- 

Serving PHP files: 
php -S localhost:8000

always runs index.php file

code.php is used for defining php statements and functions
index.php is for embedding php in HTML to be served and shown in our browser
-->
<?php require_once './code.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>S01: PHP Basics and Selection Control</title>
</head>
<body>
    <!-- <h1>Hello World!</h1> -->
    <h1>Echoing Values</h1>
    <!-- prints out the variable or code in the code.php -->
    <!-- close php tag since document combines html and php -->
    <!-- Single quote cannot output the variable values -->
    <p><?php echo 'Good day $name! Your given email is $email'; ?></p>
    <!-- Single quote can be used but concatenation operator is needed (.) -->
    <p><?php echo 'Good day ' . $name . '!' . 'Your given email is ' . $email . '.'; ?></p>
    <!-- Double quotes reads the variable automatically -->
    <p><?php echo "Good day $name! Your given email is $email"; ?></p>
    <!-- constant variable doesn't need quotes to access a variable - to concatenate with string use '' and . -->
    <p><?php echo PI; ?> </p>

    <h1>Data types</h1>
    <!-- Boolean and null doesn't show automatically -->
    <p><?php echo $hasTravelledAbroad   ?></p>
    <p><?php echo $spouse   ?></p>

    <!-- To see their types instead, we can use var_dump() function -->
    <p><?php echo gettype($hasTravelledAbroad); ?> </p>
    <p><?php echo gettype($spouse); ?></p>

    
    <p><?php echo var_dump($hasTravelledAbroad); ?> </p>
    <p><?php echo var_dump($spouse); ?></p></body>
    
    <p><?php var_dump($gradeObj); ?></p>
    <p> <?php echo $gradeObj -> firstGrading; ?></p>
    <p> <?php echo $personObj -> address -> state; ?> </p>


    <p> <?php ECHO $grades[3]; ?></p>
    <p> <?php ECHO $grades[2]; ?></p>

    <h1>Operators</h1>
    <p>X: <?php echo $x;?> </p>
    <p>Y: <?php echo $y;?> </p>

    <p>is Legal Age: <?php var_dump($isLegalAge); ?> </p>
    <p>is Registered: <?php var_dump($isRegistered); ?> </p>

    <h2>Arithmetic Operators</h2>
    <p>Sum: <?php echo $x + $y; ?></p>
    <p>Difference: <?php echo $x - $y; ?></p>
    <p>Product: <?php echo $x * $y; ?></p>
    <p>Quotient: <?php echo $x / $y; ?></p>
    <p>Modulo: <?php echo $x % $y; ?></p>

    <h2>Equality Operator</h2>
    <p>Loose Equality: <?php var_dump($x == '1342.14'); ?></p>
    <p>Strict Equality: <?php var_dump($x === '1342.14'); ?></p>

    <p>Loose Inequality: <?php var_dump($x != '1342.14'); ?></p>
    <p>Strict Inequality: <?php var_dump($x !== '1342.14'); ?></p>

    <h2>Greater/Lesser Operator</h2>

    <p>Is Lesser: <?php var_dump($x < $y); ?></p>
    <p>Is Greater: <?php var_dump($x > $y); ?></p>
    <p>Is Lesser or Equal : <?php var_dump($x <= $y); ?></p>
    <p>Is Greater or Equal : <?php var_dump($x >= $y); ?></p>


    <h2>Logical Operators</h2>
    <p>Are All Requirements Met: <?php var_dump($isLegalAge && $isRegistered); ?></p>
    <p>Some Requirements are Met: <?php var_dump($isLegalAge || $isRegistered); ?></p>
    <p>Some Requirements are Met: <?php var_dump($isLegalAge && !$isRegistered); ?></p>

    <h1>Functions</h1>

    <p>Full Name: <?php echo getFullName('John', 'D', 'Smith')?> </p>

    <h1>Selection Control Structures</h1>
    <h2>If-ElseIf-Else</h2>
    <p><?php echo determineTyphoonIntensity(12); ?></p>

    <h2>Ternary Sample</h2>
    <p> 78: <?php echo isUnderAge(78); ?></p>
    <p> 17: <?php echo isUnderAge(17); ?></p>

    <h2>Switch Case</h2>
    <p> <?php echo determineComputerUser(5); ?></p>
    <p> <?php echo determineComputerUser(10); ?></p>

    <h2>Try-Catch-Finally</h2>
    <p><?php greeting ("Hello"); ?></p>
    <p><?php greeting (5); ?></p>
</body>

</html>