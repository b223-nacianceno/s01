<!-- <?php ?> Can ommit closing tag if pure php code document-->
<?php

//[SECTION 1] Variables - container, stores data/values
// Variables are defined using dollar ($) notation before the name of the variable.

// delimiter/terminater ';' - REQUIRED

$name = 'John Smith';
$email = 'johnsmith@mail.com';

// [SECTION 2] Constants are defined using the 'define()' function.
// Naming convention for 'constants' should be in ALL CAPS.
// Doesn't use the $ notatoin before the variable.

define('PI',3.1416);

//[SECTION 3] Data Types

// Strings
$state = 'New York';
$country = 'United States of America';
// concatenation via . operator
$address = $state .', ' . $country;
// concatenation using double quotes
$address = "$state, $country";

// Integer
$age = 31;
$headcount = 26;

// Floats
$grade = 98.2;
$distanceInKilometers = 1342.12;

// Boolean
$hasTravelledAbroad = false;
$haveSymptoms = true;

// Arrays 
// 5.4 version
$grades = array (98.7, 92.1, 90.2, 94.6);
// still works
$grades = [98.7, 92.1, 90.2, 94.6];

// Null
$spouse = null;
$middleName = null;

// Objects 
$gradeObj = (object) [
    'firstGrading' => 98.7,
    'secondGrading' => 92.1,
    'thirdGrading' => 90.2,
    'fourthGrading' => 94.6
];

$personObj = (object) [
    'fullName' => 'John Smith',
    'isMarried' => false,
    'age' => 35,
    'address' => (object) [
        'state' => 'New York',
        'country' => 'United States of America'
    ]
];

// [SECTION] Operators

// Assignment Operators

$x = 1342.14;
$y = 1268.24;

$isLegalAge = true;
$isRegistered = false;

// [SECTION] Functions
function getFullName ($firstName, $middleInitial, $lastName){
    return "$lastName, $firstName $middleInitial";
}

// If-ElseIf-Else Statement

function determineTyphoonIntensity($windSpeed){
    if ($windSpeed <30 ) {
        return 'Not a typhoon yet.';
    }
    else if ($windSpeed <= 61) {
        return 'Tropical Depression detected.';
    } else if ($windSpeed >= 62 && $windSpeed <= 88) {
        return 'Tropical Storm detected.';
    } else if ($windSpeed >= 89 && $windSpeed <= 117) {
        return 'Severe Tropical Storm detected.';
    } else {
        return 'Typhoon Detected.';
    }
}

// Conditional (Ternary) Operator
function isUnderAge($age){
    return ($age < 18) ? 'is under age.' : 'is legal age.';
}

// Switch Statement
function determineComputerUser ($computerNumber){
    switch ($computerNumber) {
        case 1:
            return 'Linus Torvalds';
            break;
        
        case 2:
            return 'Steve Jobs';
            break;
        case 3:
            return 'Sid Meier';
            break;
        case 4:
            return 'Onel De Guzman';
            break;
        case 5:
            return 'Christian Salvador';
            break;
        default:
        return $computerNumber . ' is out of bonds.';
    }
}

//Try-Catch-Finally Statement

function greeting ($str){
    try{
        if(gettype($str) === 'string'){
            echo $str;
        }
        else {
            throw new Exception("Oops!");
        }
    }
    catch (Exception $e) {
        echo $e-> getMessage();
    } finally {
        echo " I did it again!";
    }
}